import "./App.css";
import { StyledWrapper } from "./components/StyledWrapper";
import CustomForm from "./components/CustomForm";

const App = () => {
  return (
    <StyledWrapper>
      <CustomForm />
    </StyledWrapper>
  );
};

export default App;
