import { UseFormGetValues } from "react-hook-form";
import { IFormInput } from "../components/CustomForm";

export const validateFields = (getValues: UseFormGetValues<IFormInput>) => {
    const { letter, firstInput } = getValues();
    return letter !== "c" || firstInput.length >= 5;
};