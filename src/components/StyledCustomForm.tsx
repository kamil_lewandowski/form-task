import styled, { keyframes } from "styled-components";

const appear = keyframes`
  from {
    opacity:0;
    transform:translateY(20px);
  }

  to {
    opacity:1;
    transform:translateY(0);
  }
`;

export const StyledCustomForm = styled.form`
  animation: ${appear} 1s ease;
  max-width: 400px;
  width: 80%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  gap: 1.5rem;
  padding: 50px;
  border: 1px solid #eee;
  box-shadow: 0 2px 3px #ccc;
  border-radius: 5px;
`;
