import { Select, Space, Typography } from "antd";
import { Controller, useFormContext } from "react-hook-form";

const { Text } = Typography;

export interface IOption {
  label: string;
  value: string | number;
}

interface CustomSelectProps {
  name: string;
  defaultValue: string | number;
  errorMessage?: string;
  rules?: object;
  options: IOption[];
}

const CustomSelect = ({
  options,
  errorMessage,
  ...config
}: CustomSelectProps) => {
  const { control } = useFormContext();

  return (
    <Controller
      {...config}
      control={control}
      render={({ field, fieldState: { error } }) => (
        <Space direction="vertical">
          <Select {...field} options={options} style={{ width: "100%" }} />
          {error && <Text type="danger">{errorMessage}</Text>}
        </Space>
      )}
    />
  );
};

export default CustomSelect;
