import { Input, Space, Typography } from "antd";
import { Controller, useFormContext } from "react-hook-form";

const { Text } = Typography;

interface CustomInputProps {
  name: string;
  defaultValue: string;
  errorMessage?: string;
  rules?: object;
}

const CustomInput = ({ errorMessage, ...config }: CustomInputProps) => {
  const { control } = useFormContext();
  return (
    <Controller
      {...config}
      control={control}
      render={({ field, fieldState: { error } }) => (
        <Space direction="vertical">
          <Input {...field} />
          {error && <Text type="danger">{errorMessage}</Text>}
        </Space>
      )}
    />
  );
};

export default CustomInput;
