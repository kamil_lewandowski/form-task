import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { message } from "antd";
import CustomForm from "./CustomForm";

test("invoke success notification when form inputs meet criteria", async () => {
  // for consistency I could use fireEvent.change(<input>, {target: {value: <value>}})
  // but it's easier to simulate typing with userEvent
  render(<CustomForm />);
  const messageMock = jest.spyOn(message, "success");
  const [firstInput, secondInput] = screen.getAllByRole("textbox");
  const dropdown = screen.getByRole("combobox");
  const submitBtn = screen.getByRole("button");
  userEvent.type(firstInput, "12345");
  userEvent.type(secondInput, "1");
  // fireEvent.mouseDown instead of userEvent.selectOptions because of bug
  // https://github.com/ant-design/ant-design/issues/23009
  fireEvent.mouseDown(dropdown);
  const optionC = await screen.findByText("C");
  // here again useEvent wouldn't work
  fireEvent.click(optionC);

  userEvent.click(submitBtn);

  await waitFor(() => {
    expect(messageMock).toHaveBeenCalled();
  });
});

describe("when required input is not filled", () => {
  test("don't show success notification", async () => {
    render(<CustomForm />);
    const submitBtn = screen.getByRole("button");
    const messageMock = jest.spyOn(message, "success");

    userEvent.click(submitBtn);

    await waitFor(
      () => {
        expect(messageMock).not.toHaveBeenCalled();
      },
      { timeout: 2000 }
    );
  });

  test("display error message", async () => {
    render(<CustomForm />);
    const submitBtn = screen.getByRole("button");

    userEvent.click(submitBtn);

    const errorMessage = await screen.findByText(/to pole jest wymagane/i);
    expect(errorMessage).toBeInTheDocument();
  });
});

describe("when custom validation fails", () => {
  test("don't show success notification", async () => {
    render(<CustomForm />);
    const messageMock = jest.spyOn(message, "success");
    const [firstInput, secondInput] = screen.getAllByRole("textbox");
    const dropdown = screen.getByRole("combobox");
    const submitBtn = screen.getByRole("button");
    userEvent.type(firstInput, "1234");
    userEvent.type(secondInput, "1");
    fireEvent.mouseDown(dropdown);
    const optionC = await screen.findByText("C");
    fireEvent.click(optionC);

    userEvent.click(submitBtn);

    await waitFor(
      () => {
        expect(messageMock).not.toHaveBeenCalled();
      },
      { timeout: 2000 }
    );
  });

  test("display error message", async () => {
    render(<CustomForm />);
    const [firstInput, secondInput] = screen.getAllByRole("textbox");
    const dropdown = screen.getByRole("combobox");
    const submitBtn = screen.getByRole("button");
    userEvent.type(firstInput, "1234");
    userEvent.type(secondInput, "1");
    fireEvent.mouseDown(dropdown);
    const optionC = await screen.findByText("C");
    fireEvent.click(optionC);

    userEvent.click(submitBtn);

    const errorMessage = await screen.findByText(/nie spełnione kryteria/i);
    expect(errorMessage).toBeInTheDocument();
  });
});
