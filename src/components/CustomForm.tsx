import { useForm, SubmitHandler, FormProvider } from "react-hook-form";
import { Button, message } from "antd";
import CustomInput from "./CustomInput";
import CustomSelect, { IOption } from "./CustomSelect";
import { StyledCustomForm } from "./StyledCustomForm";
import { validateFields } from "../utils";

export interface IFormInput {
  letter: string;
  firstInput: string;
  secondInput: string;
}

const selectOptions: IOption[] = [
  { label: "A", value: "a" },
  { label: "B", value: "b" },
  { label: "C", value: "c" },
];

const CustomForm = () => {
  const methods = useForm<IFormInput>({ reValidateMode: "onSubmit" });
  const { handleSubmit, getValues } = methods;

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    console.log(data);
    message.success("sukces");
  };

  return (
    <FormProvider {...methods}>
      <StyledCustomForm onSubmit={handleSubmit(onSubmit)}>
        <CustomSelect
          name="letter"
          defaultValue={selectOptions[0].value}
          errorMessage="Nie spełnione kryteria"
          options={selectOptions}
          rules={{
            validate: validateFields.bind(null, getValues),
          }}
        />
        <CustomInput name="firstInput" defaultValue="" />
        <CustomInput
          name="secondInput"
          defaultValue=""
          rules={{ required: true }}
          errorMessage="To pole jest wymagane"
        />
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </StyledCustomForm>
    </FormProvider>
  );
};

export default CustomForm;
